//
//  AlamofireAdaptor.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit
import Moya

final class MoyaAdaptor: NetworkingInterface {
    let provider: MoyaProvider<RequestSpecs>
    
    init(provider: MoyaProvider<RequestSpecs>) {
        self.provider = provider
    }
    
    func request<T: Decodable>(_ specs: RequestSpecs, completion: @escaping (T?, Error?) -> Void) {
        provider.request(specs) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(T.self, from: response.data)
                    completion(results, nil)
                } catch let error {
                    completion(nil, error)
                }
            case let .failure(error):
                completion(nil, error)
            }
        }
    }
}
