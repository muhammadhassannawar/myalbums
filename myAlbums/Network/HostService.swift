//
//  HostService.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

struct HostService {
    // MARK: - static Properties And Methods
    static func getBaseURL() -> String {
        return "https://jsonplaceholder.typicode.com/"
    }
    
    static func getAlbumsURL() -> String {
        return getBaseURL() + "albums"
    }
    
    static func getUserURL(id: Int) -> String {
        return getBaseURL() + "users/\(id)"
    }
    
    static func getPhotosURL() -> String {
        return getBaseURL() + "photos"
    }
}
