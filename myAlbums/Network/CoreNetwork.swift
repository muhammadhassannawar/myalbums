//
//  CoreNetwork.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit
import Moya

class CoreNetwork {
    // MARK: - Properties
    private let  provider = MoyaProvider<RequestSpecs>(plugins: [NetworkLoggerPlugin()])
    fileprivate var networkCommunication: NetworkingInterface {
        return MoyaAdaptor(provider: provider)
    }
}

// MARK: - Core Network Protocol methods
extension CoreNetwork: CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs,
                                 completion: @escaping (T?, Error?) -> Void) {
        networkCommunication.request(request, completion: completion)
    }
}

protocol CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs,
                                 completion: @escaping (T?, Error?) -> Void)
}
