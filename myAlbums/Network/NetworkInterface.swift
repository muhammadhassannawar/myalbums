//
//  NetworkInterface.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation
import Moya

/// Request specification to collect all related data to perform a request which contain method type, url and parameters
enum RequestSpecs {
    case user(_ id: Int)
    case albums(_ userID: Int)
    case photos(_ albumID: Int)
}


extension RequestSpecs: TargetType {
    
    var baseURL: URL {
        switch self {
        case .user(let id):
            guard let url = URL(string: HostService.getUserURL(id: id)) else { fatalError() }
            return url
            
        case .albums(_ ):
            guard let url = URL(string: HostService.getAlbumsURL()) else { fatalError() }
            return url
            
        case .photos(_ ):
            guard let url = URL(string: HostService.getPhotosURL()) else { fatalError() }
            return url
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        .get
    }
    
    var task: Task {
        switch self {
        case .user(_ ):
            return .requestPlain
            
        case .albums(let userID):
            var params: [String: Any] = [:]
            params["userId"] = userID
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            
        case .photos(let albumID):
            var params: [String: Any] = [:]
            params["albumId"] = albumID
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
}

protocol NetworkingInterface {
    var provider: MoyaProvider<RequestSpecs> { get }
    func request<T: Decodable>(_ specs: RequestSpecs, completion: @escaping (T?, Error?) -> Void)
}
