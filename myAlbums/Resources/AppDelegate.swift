//
//  AppDelegate.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let wireFrame = MainWireFrame()
        guard let MainViewController = MainModule().configure(wireframe: wireFrame) else { return true }
        let navigationController = UINavigationController(rootViewController: MainViewController)
        wireFrame.parentNavigationController = navigationController
        window.rootViewController = navigationController
        self.window = window
        window.makeKeyAndVisible()
        return true
    }
}
