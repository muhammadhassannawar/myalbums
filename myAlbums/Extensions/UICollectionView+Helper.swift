//
//  UICollectionView+Helper.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

extension UICollectionView {
    func register<T: UICollectionViewCell>(_ cellType: T.Type) {
        let name = String(describing: cellType)
        let nib = UINib(nibName: name, bundle: nil)
        register(nib,
                 forCellWithReuseIdentifier: name)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T? {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.id, for: indexPath) as? T else {
            fatalError("\(T.self) is expected to have reusable identifier: \(T.id)")
        }
        return cell
    }
}

extension UICollectionViewCell {
    class var id: String {
        return "\(self)"
    }
}
