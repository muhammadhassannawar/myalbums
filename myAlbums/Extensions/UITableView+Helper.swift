//
//  UITableView+Helper.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

// MARK: Register and dequeue table view cell
extension UITableView {
    func register<T: UITableViewCell>(_ cellType: T.Type) {
        let name = String(describing: cellType)
        let nib = UINib(nibName: name, bundle: nil)
        register(nib, forCellReuseIdentifier: name)
    }
    
    func dequeueHeader<T: UITableViewCell>() -> T {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.id) as? T else {
            fatalError("\(T.self) is expected to have reusable identifier: \(T.id)")
        }
        return cell
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.id, for: indexPath) as? T else {
            fatalError("\(T.self) is expected to have reusable identifier: \(T.id)")
        }
        return cell
    }
}

// MARK: Get cell identifier and cell name
extension UITableViewCell {
    class var id: String {
        return "\(self)"
    }
}
