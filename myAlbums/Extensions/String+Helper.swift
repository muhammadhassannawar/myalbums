//
//  String+Helper.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

// MARK: - extension to provide default value to optional String
public extension Optional where Wrapped == String {
    var value: String {
        return self ?? ""
    }
}

extension String {
    static var comaWithSpace: String {
        return ", "
    }
    
    static var empty: String {
        return ""
    }
}
