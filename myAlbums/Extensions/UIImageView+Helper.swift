//
//  UIImageView+Helper.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import SDWebImage

extension UIImageView {
    func downloadImageWithProgress(_ url: URL, placeholderImage: UIImage? = nil) {
        self.configureLoadingIndicator()
        sd_setImage(with: url, placeholderImage: placeholderImage ?? image) {
            [weak self]  (image, error, cacheType, imageURL) in
            guard let strongSelf = self, let _ = image else { return }
            strongSelf.stopLoadingAnimation()
        }
        if let _ = self.image {
            self.stopLoadingAnimation()
        }
    }
    
    func stopLoading() {
        sd_cancelCurrentImageLoad()
        stopLoadingAnimation()
    }
    
    func stopLoadingAnimation() {
        guard let currentIndicator = subviews.first as? UIActivityIndicatorView else { return }
        currentIndicator.stopAnimating()
    }
    
    fileprivate func configureLoadingIndicator() {
        if let currentIndicator = subviews.first as? UIActivityIndicatorView {
            currentIndicator.startAnimating()
            return
        }
        addIndicatorToTheCenter().startAnimating()
    }
    
    private func addIndicatorToTheCenter() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        self.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicator
    }
}

extension UIImageView {
  func enableZoom() {
    let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(startZooming(_:)))
    isUserInteractionEnabled = true
    addGestureRecognizer(pinchGesture)
  }

  @objc
  private func startZooming(_ sender: UIPinchGestureRecognizer) {
    let scaleResult = sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)
    guard let scale = scaleResult, scale.a > 1, scale.d > 1 else { return }
    sender.view?.transform = scale
    sender.scale = 1
  }
}
