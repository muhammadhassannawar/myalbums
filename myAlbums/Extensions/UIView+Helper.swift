//
//  UIView+Helper.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

extension UIView {
    /// A property that accesses the frame.size.width property.
    @IBInspectable
    open var width: CGFloat {
        get {
            return frame.size.width
        }
        set(value) {
            frame.size.width = value
        }
    }
    
    /// A property that accesses the frame.size.height property.
    @IBInspectable
    open var height: CGFloat {
        get {
            return frame.size.height
        }
        set(value) {
            frame.size.height = value
        }
    }
}
