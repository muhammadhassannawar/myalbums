//
//  Int+Helper.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

// MARK: - extension to provide default value to optional Int
public extension Optional where Wrapped == Int {
    var value: Int {
        return self ?? 0
    }
}
