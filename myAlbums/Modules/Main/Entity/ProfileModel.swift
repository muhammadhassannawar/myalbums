//
//  ProfileModel.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

// MARK: - User
struct User: Codable {
    let id: Int
    let name: String
    let address: Address
    
}

// MARK: - Address
struct Address: Codable {
    var street, suite, city, zipCode: String?
    
    enum CodingKeys: String, CodingKey {
        case zipCode = "zipcode"
        case street, suite, city
    }
}

extension Address {
    var fullAddress: String? {
        guard let street = self.street,
              let suite = self.suite,
              let city = self.city,
              let zipCode = zipCode else { return nil }
        return street + .comaWithSpace + suite + .comaWithSpace + city + .comaWithSpace + zipCode
    }
}

// MARK: - Album
struct Album: Codable {
    let id: Int
    let title: String
}

// MARK: - HeaderInfo
struct HeaderInfo {
    let userName: String
    let userAddress: String
    let albumTitle: String
}
