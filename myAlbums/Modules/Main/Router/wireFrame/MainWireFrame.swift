//
//  MainWireFrame.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

protocol MainWireFrameProtocol {
    var parentNavigationController: UINavigationController? { get }
    func showAlbumPhotosScreen(_ album: Album)
}

class MainWireFrame: MainWireFrameProtocol {
    var parentNavigationController: UINavigationController?
    func showAlbumPhotosScreen(_ album: Album) {
        AlbumPhotosWireFrame().showAlbumPhotosScreen(in: parentNavigationController, album: album)
    }
}
