//
//  MainModule.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

class MainModule {
    func configure(wireframe: MainWireFrame? = nil) -> MainView? {
        guard let viewController = MainView.loadFromNib() else { return nil }
        viewController.viewModel = MainViewModel(wireFrame: wireframe)
        return viewController
    }
}
