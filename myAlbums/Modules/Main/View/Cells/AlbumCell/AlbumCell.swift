//
//  AlbumCell.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

protocol AlbumCellView {
    func configure(_ title: String)
}

final class AlbumCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
}

// MARK: - View Protocol
extension AlbumCell: AlbumCellView {
    func configure(_ title: String) {
        titleLabel.text = title
    }
}
