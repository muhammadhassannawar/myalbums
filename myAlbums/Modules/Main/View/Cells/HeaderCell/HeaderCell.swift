//
//  HeaderCell.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

protocol HeaderCellView {
    func configure(_ headerInfo: HeaderInfo)
}

final class HeaderCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userAddressLabel: UILabel!
}

// MARK: - View Protocol
extension HeaderCell: HeaderCellView {
    func configure(_ headerInfo: HeaderInfo) {
        titleLabel.text = headerInfo.albumTitle
        userNameLabel.text = headerInfo.userName
        userAddressLabel.text = headerInfo.userAddress
    }
}
