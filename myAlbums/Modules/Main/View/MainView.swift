//
//  MainView.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit
import Combine

final class MainView: UIViewController {
    // MARK: - Outlets & Properties
    @IBOutlet weak private var loadingView: UIActivityIndicatorView!
    @IBOutlet weak private var ProfileTableView: UITableView!{
        didSet {
            setupTableView(ProfileTableView, AlbumCell.self)
            setupTableViewHeader(ProfileTableView, HeaderCell.self)
        }
    }
    var viewModel = MainViewModel()
    private var cancellable = Set<AnyCancellable>()
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = viewModel.title
        self.setupNavigationBar()
        self.viewModel.viewDidLoad()
        binding()
    }
    
    // MARK: - Combine
    private func binding() {
        viewModel.$isLoading
            .sink(receiveValue: { [unowned self] isLoading in
                isLoading ? self.showLoadingView() : self.hideLoadingView()
            }
            )
            .store(in: &cancellable)
        
        viewModel.$reloadComponent
            .sink(receiveValue: { [unowned self] _ in
                self.ProfileTableView.reloadData()
            }
            )
            .store(in: &cancellable)
        
        viewModel.$errorMessage
            .sink(receiveValue: {[unowned self] errorMessage in
                guard let message = errorMessage, !message.isEmpty else { return }
                self.showMessageAlert(message)
            }
            )
            .store(in: &cancellable)
    }
    
    private func hideLoadingView() {
        self.ProfileTableView.reloadData()
        loadingView.stopAnimating()
    }
    
    private func showLoadingView() {
        loadingView.startAnimating()
    }
}

// MARK: - UITableView Data Source And Delegate
extension MainView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return getConfiguredHeaderCell(tableView: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getConfiguredAlbumCell(tableView: tableView, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectRowInAlbums(index: indexPath.row)
    }
    
    private func getConfiguredAlbumCell(tableView: UITableView, for index: IndexPath) -> UITableViewCell {
        let cellViewModel = viewModel.getCellViewModel(index: index.row)
        let cell: AlbumCell? = tableView.dequeueReusableCell(for: index)
        cell?.configure(cellViewModel)
        return cell ?? UITableViewCell()
    }
    
    private func getConfiguredHeaderCell(tableView: UITableView) -> UITableViewCell {
        let cell: HeaderCell? = tableView.dequeueHeader()
        cell?.configure(viewModel.headerInfo)
        return cell ?? UITableViewCell()
    }
    
    private func setupTableView(_ tableView: UITableView,_ cellType: UITableViewCell.Type) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.keyboardDismissMode = .onDrag
        tableView.tableFooterView = UIView()
        tableView.register(cellType)
        tableView.estimatedRowHeight = UITableView.automaticDimension
    }
    
    private func setupTableViewHeader(_ tableView: UITableView,_ cellType: UITableViewCell.Type) {
        tableView.register(cellType)
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 140
    }
}
