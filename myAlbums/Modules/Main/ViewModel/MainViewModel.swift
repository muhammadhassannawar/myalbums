//
//  MainViewModel.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

final class MainViewModel {
    // MARK: - Private Properties
    private let interactor: MainInteractorProtocol
    private let wireFrame: MainWireFrameProtocol?
    private var albumsModel: [Album] = []
    private var userInfoModel: User?
    
    @Published var isLoading = false
    @Published var errorMessage: String?
    @Published var reloadComponent = false
    var headerInfo: HeaderInfo {
        return HeaderInfo(userName: (userInfoModel?.name).value,
                          userAddress: (userInfoModel?.address.fullAddress).value,
                          albumTitle: "My Albums")
    }
    var numberOfCells: Int {
        return albumsModel.count
    }
    var title: String {
        return "Profile"
    }
    
    init(interactor: MainInteractorProtocol = MainInteractor(),
         wireFrame: MainWireFrameProtocol? = nil) {
        self.interactor = interactor
        self.wireFrame = wireFrame
    }
    
    // MARK: - Private methods
    private func loadAlbums(userID: Int) {
        isLoading = true
        interactor.loadUserAlbums(userID){ [unowned self] (result, error) in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = error
                }
            } else if let result = result {
                self.albumsModel = result
            }
            isLoading = false
            self.reloadComponent = true
        }
    }
    
    private func loadUserInfo(userID: Int) {
        isLoading = true
        interactor.loadUserInfo(userID) { [unowned self] (result, error) in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = error
                }
            } else if let result = result {
                self.userInfoModel = result
            }
            isLoading = false
            self.reloadComponent = true
        }
    }
}

// MARK: - viewModel Protocol methods
extension MainViewModel: MainViewModelProtocol {
    func viewDidLoad() {
        loadAlbums(userID: 1)
        loadUserInfo(userID: 1)
    }
    
    func didSelectRowInAlbums(index: Int) {
        wireFrame?.showAlbumPhotosScreen(albumsModel[index])
    }
    
    func getCellViewModel(index: Int) -> String {
        return albumsModel[index].title
    }
}
