//
//  MainViewModelProtocol.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Combine

protocol BaseViewModelProtocol: ObservableObject {
    var numberOfCells: Int { get }
    var errorMessage: String? { get }
    var title: String { get }
    var isLoading: Bool { get }
    var reloadComponent: Bool { get }
}

protocol MainViewModelProtocol: BaseViewModelProtocol {
    var headerInfo: HeaderInfo { get }
    func viewDidLoad()
    func didSelectRowInAlbums(index: Int)
    func getCellViewModel(index: Int) -> String
}
