//
//  MainInteractor.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

protocol MainInteractorProtocol {
    func loadUserInfo(_ userID: Int, completion: @escaping (User?, String?) -> Void)
    func loadUserAlbums(_ userID: Int, completion: @escaping ([Album]?, String?) -> Void)
}

final class MainInteractor {
    let coreNetwork: CoreNetworkProtocol
    
    init(coreNetwork: CoreNetworkProtocol = CoreNetwork()) {
        self.coreNetwork = coreNetwork
    }
}
// MARK: - Interactor Protocol methods
extension MainInteractor: MainInteractorProtocol {
    func loadUserInfo(_ userID: Int, completion: @escaping (User?, String?) -> Void) {
        coreNetwork.makeRequest(request: .user(userID), completion: { model, error in
            completion(model, error?.localizedDescription)
        })
    }
    
    func loadUserAlbums(_ userID: Int, completion: @escaping ([Album]?, String?) -> Void) {
        coreNetwork.makeRequest(request: .albums(userID), completion: { model, error in
            completion(model, error?.localizedDescription)
        })
    }
}
