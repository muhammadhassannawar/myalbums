//
//  PhotoDetailsViewModel.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

protocol PhotoDetailsViewModelProtocol {
    var photo: String { get }
    func didPressShare()
    func didPressDismiss()
}

final class PhotoDetailsViewModel: PhotoDetailsViewModelProtocol {
    // MARK: - Private Properties
    private let wireFrame: PhotoDetailsWireFrameProtocol
    var photo: String
    
    init(photo: String,
         wireFrame: PhotoDetailsWireFrameProtocol) {
        self.photo = photo
        self.wireFrame = wireFrame
    }
    
    func didPressShare() {
        wireFrame.showSharePhoto(url: photo)
    }
    
    func didPressDismiss() {
        wireFrame.dismissPhotoView()
    }
}
