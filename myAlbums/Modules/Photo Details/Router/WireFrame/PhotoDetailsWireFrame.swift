//
//  PhotoDetailsWireFrame.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

protocol PhotoDetailsWireFrameProtocol {
    var parentNavigationController: UINavigationController? { get }
    func PresentPhotoDetailsScreen(in parentNavigationController: UINavigationController?, photo: String)
    func showSharePhoto(url: String)
    func dismissPhotoView()
}

class PhotoDetailsWireFrame: PhotoDetailsWireFrameProtocol {
    var parentNavigationController: UINavigationController?
    
    func PresentPhotoDetailsScreen(in parentNavigationController: UINavigationController?, photo: String) {
        guard let parentNavigationController = parentNavigationController ?? self.parentNavigationController,
              let controller = PhotoDetailsModule().configure(parentNavigationController: parentNavigationController, photo: photo) else { return }
        self.parentNavigationController = parentNavigationController
        self.parentNavigationController?.present(controller, animated: true, completion: nil)
    }
    
    func showSharePhoto(url: String) {
        if var topController = UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.rootViewController  {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            if let link = NSURL(string: url) {
                let objectsToShare = [link] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                topController.present(activityVC, animated: true, completion: nil)
            }
        }
    }
    func dismissPhotoView() {
        self.parentNavigationController?.dismiss(animated: true, completion: nil)
    }
}
