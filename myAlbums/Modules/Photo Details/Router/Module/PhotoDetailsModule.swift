//
//  PhotoDetailsModule.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

class PhotoDetailsModule {
    func configure(parentNavigationController: UINavigationController? = nil, photo: String) -> PhotoDetailsView? {
        guard let viewController = PhotoDetailsView.loadFromNib() else { return nil }
        let wireframe = PhotoDetailsWireFrame()
        wireframe.parentNavigationController = parentNavigationController
        let viewModel = PhotoDetailsViewModel(photo: photo, wireFrame: wireframe)
        viewController.viewModel = viewModel
        return viewController
    }
}
