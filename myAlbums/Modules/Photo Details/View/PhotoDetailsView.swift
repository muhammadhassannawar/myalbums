//
//  PhotoDetailsView.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

class PhotoDetailsView: UIViewController {
    // MARK: - Outlets & Properties
    var viewModel: PhotoDetailsViewModelProtocol?
    @IBOutlet weak var photoImageView: UIImageView!
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        photoImageView.enableZoom()
        if let imageUrl = viewModel?.photo {
            photoImageView.sd_setImage(with: URL(string: imageUrl), completed: nil)
        }
    }
    
    @IBAction func sharePhoto(_ sender: Any) {
        viewModel?.didPressShare()
    }
    
    @IBAction func dismissView(_ sender: Any) {
        viewModel?.didPressDismiss()
    }
}
