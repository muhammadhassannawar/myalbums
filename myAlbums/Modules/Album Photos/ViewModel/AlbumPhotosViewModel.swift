//
//  AlbumPhotosViewModel.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

final class AlbumPhotosViewModel {
    // MARK: - Private Properties
    private let interactor: AlbumPhotosInteractorProtocol
    private let wireFrame: AlbumPhotosWireFrameProtocol
    private var album: Album
    private var searchText = ""
    private var photosModel: [AlbumPhoto] = []
    
    @Published var isLoading = false
    @Published var errorMessage: String?
    @Published var reloadComponent = false
    var numberOfCells: Int {
        return getPhotosCount()
    }
    var title: String {
        return album.title
    }
    
    init(interactor: AlbumPhotosInteractorProtocol = AlbumPhotosInteractor(),
         album: Album,
         wireFrame: AlbumPhotosWireFrame) {
        self.wireFrame = wireFrame
        self.interactor = interactor
        self.album = album
    }
    
    // MARK: - Private methods
    private func loadAlbums(userID: Int) {
        interactor.loadUserAlbumPhotos(album.id){ [unowned self] (result, error) in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = error
                }
            } else if let result = result {
                self.photosModel = result
            }
            isLoading = false
            self.reloadComponent = true
        }
    }
    
    private func getPhotosCount() -> Int {
        if searchText.isEmpty {
            return photosModel.count
        } else {
            let filtered = photosModel.filter { $0.title.contains(searchText)}
            return filtered.count
        }
    }
}

// MARK: - ViewModel Protocol methods
extension AlbumPhotosViewModel: AlbumPhotosViewModelProtocol {
    func searchBy(searchText: String) {
        self.searchText = searchText
        self.reloadComponent = true
    }
    
    func cancelSearch() {
        searchText = ""
        self.reloadComponent = true
    }
    
    func viewDidLoad() {
        isLoading = true
        loadAlbums(userID: 1)
    }
    
    func didSelectItemInCollection(index: Int) {
        wireFrame.presentPhotoDetailsScreen(photosModel[index].url)
    }
    
    func getCellViewModel(at indexPath: IndexPath) -> String {
        if searchText.isEmpty {
            return photosModel[indexPath.row].url
        } else {
            let filtered = photosModel.filter { $0.title.contains(searchText)
            }
            return filtered[indexPath.row].url
        }
    }
}
