//
//  AlbumPhotosViewModelProtocol.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

protocol AlbumPhotosViewModelProtocol: BaseViewModelProtocol {
    func viewDidLoad()
    func getCellViewModel( at indexPath: IndexPath ) -> String
    func searchBy(searchText: String)
    func cancelSearch()
    func didSelectItemInCollection(index: Int)
}
