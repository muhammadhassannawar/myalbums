//
//  PhotoModel.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

// MARK: - AlbumPhoto
struct AlbumPhoto: Codable {
    let title: String, url: String
}
