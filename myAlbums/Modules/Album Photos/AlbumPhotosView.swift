//
//  AlbumPhotosView.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit
import Combine

class AlbumPhotosView: UIViewController {
    // MARK: - Outlets & Properties
    @IBOutlet weak var photosCollction: UICollectionView! {
        didSet {
            self.setupCollectionView(photosCollction, PhotoCell.self)
        }
    }
    @IBOutlet weak private var searchTermsField: UISearchBar!
    
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    var viewModel: AlbumPhotosViewModel?
    private var cancellable = Set<AnyCancellable>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel?.title
        setupSearchBar()
        self.viewModel?.viewDidLoad()
        binding()
    }
    
    // MARK: - Combine
    private func binding() {
        viewModel?.$isLoading
            .sink(receiveValue: { [unowned self] isLoading in
                isLoading ? self.showLoadingView() : self.hideLoadingView()
            }
            )
            .store(in: &cancellable)
        
        viewModel?.$reloadComponent
            .sink(receiveValue: { [unowned self] _ in
                self.photosCollction.reloadData()
            }
            )
            .store(in: &cancellable)
        
        viewModel?.$errorMessage
            .sink(receiveValue: {[unowned self] errorMessage in
                guard let message = errorMessage, !message.isEmpty else { return }
                self.showMessageAlert(message)
            }
            )
            .store(in: &cancellable)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        guard let columnLayout = photosCollction.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        columnLayout.invalidateLayout()
    }
    
    // MARK: - Private methods
    private func hideLoadingView() {
        self.photosCollction.reloadData()
        loadingView.stopAnimating()
    }
    
    private func showLoadingView() {
        loadingView.startAnimating()
    }
    
    private func cancelSearch() {
        self.searchTermsField.resignFirstResponder()
        self.searchTermsField.text = ""
        viewModel?.cancelSearch()
    }
}

// MARK: - UICollection View Delegate Flow Layout
extension AlbumPhotosView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Double((collectionView.width - (collectionView.contentInset.left +  collectionView.contentInset.right))/3)
        return CGSize(width: width, height: width)
    }
}

// MARK: - UICollection View Setup And DataSource
extension AlbumPhotosView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return (viewModel?.numberOfCells).value
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getConfiguredCell(collectionView, cellForItemAt: indexPath)
    }
    
    func getConfiguredCell(_ collectionView: UICollectionView,
                           cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getConfiguredPhotoCell(collectionView: collectionView, for: indexPath)
    }
    
    private func getConfiguredPhotoCell(collectionView: UICollectionView,
                                        for index: IndexPath) -> UICollectionViewCell {
        guard let cellViewModel = viewModel?.getCellViewModel(at: index),
              let cell: PhotoCell = collectionView.dequeueReusableCell(for: index) else { return UICollectionViewCell()}
        cell.configure(cellViewModel)
        return cell
    }
    
    private func setupCollectionView(_ collectionView: UICollectionView,
                                     _ cellType: UICollectionViewCell.Type) {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(cellType)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel?.didSelectItemInCollection(index: indexPath.item)
    }
}

// MARK: - UI Search Bar Setup And Delegate
extension AlbumPhotosView: UISearchBarDelegate {
    private func setupSearchBar() {
        searchTermsField.delegate = self
        searchTermsField.barTintColor = UIColor.clear
        if #available(iOS 13.0, *) {
            searchTermsField.searchTextField.textColor = .black
        }
        searchTermsField.backgroundColor = UIColor.clear
        searchTermsField.isTranslucent = true
        searchTermsField.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchTermsField.setImage(UIImage(), for: .clear, state: .normal)
        searchTermsField.showsCancelButton = true
        let toolbar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .done,
                                         target: self, action: #selector(clickCancelButtonOnKeyboard))
        toolbar.setItems([flexSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        searchTermsField.inputAccessoryView = toolbar
    }
    
    @objc func clickCancelButtonOnKeyboard() {
        cancelSearch()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text?.lowercased() else { return }
        self.searchTermsField.resignFirstResponder()
        viewModel?.searchBy(searchText: text)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text?.lowercased() else { return }
        viewModel?.searchBy(searchText: text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        cancelSearch()
    }
}
