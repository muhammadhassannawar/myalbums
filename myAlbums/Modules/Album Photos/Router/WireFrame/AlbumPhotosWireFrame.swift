//
//  AlbumPhotosWireFrame.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

protocol AlbumPhotosWireFrameProtocol {
    var parentNavigationController: UINavigationController? { get }
    func showAlbumPhotosScreen(in parentNavigationController: UINavigationController?, album: Album)
    func presentPhotoDetailsScreen(_ photo: String)
}

class AlbumPhotosWireFrame: AlbumPhotosWireFrameProtocol {
    var parentNavigationController: UINavigationController?
    func showAlbumPhotosScreen(in parentNavigationController: UINavigationController?, album: Album) {
        guard let parentNavigationController = parentNavigationController ?? self.parentNavigationController,
              let controller = AlbumPhotosModule().configure(parentNavigationController: parentNavigationController, album: album) else { return }
        self.parentNavigationController = parentNavigationController
        self.parentNavigationController?.pushViewController(controller, animated: true)
    }
    
    func presentPhotoDetailsScreen(_ photo: String) {
        PhotoDetailsWireFrame().PresentPhotoDetailsScreen(in: parentNavigationController, photo: photo)
    }
}
