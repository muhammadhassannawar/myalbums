//
//  AlbumPhotosModule.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit

class AlbumPhotosModule {
    func configure(parentNavigationController: UINavigationController? = nil, album: Album) -> AlbumPhotosView? {
        guard let viewController = AlbumPhotosView.loadFromNib() else { return nil }
        let wireframe = AlbumPhotosWireFrame()
        wireframe.parentNavigationController = parentNavigationController
        let viewModel = AlbumPhotosViewModel(album: album, wireFrame: wireframe)
        viewController.viewModel = viewModel
        return viewController
    }
}
