//
//  AlbumPhotosInteractor.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import Foundation

protocol AlbumPhotosInteractorProtocol {
    func loadUserAlbumPhotos(_ albumID: Int, completion: @escaping ([AlbumPhoto]?, String?) -> Void)
}

final class AlbumPhotosInteractor {
    let coreNetwork: CoreNetworkProtocol
    
    init(coreNetwork: CoreNetworkProtocol = CoreNetwork()) {
        self.coreNetwork = coreNetwork
    }
}
// MARK: - Interactor Protocol methods
extension AlbumPhotosInteractor: AlbumPhotosInteractorProtocol {
    func loadUserAlbumPhotos(_ albumID: Int, completion: @escaping ([AlbumPhoto]?, String?) -> Void) {
        coreNetwork.makeRequest(request: .photos(albumID), completion: { model, error in
            completion(model, error?.localizedDescription)
        })
    }
}
