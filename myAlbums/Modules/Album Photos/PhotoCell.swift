//
//  PhotoCell.swift
//  myAlbums
//
//  Created by Mohamed Hassan Nawar on 27/05/2022.
//

import UIKit
import SDWebImage

protocol PhotoCellView {
    func configure(_ url: String)
}

final class PhotoCell: UICollectionViewCell {
    // MARK: - Outlets
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.stopLoading()
    }
}

// MARK: - View Protocol
extension PhotoCell: PhotoCellView {
    func configure(_ url: String) {
        guard let url = URL(string: url) else { return }
        photoImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photoImageView.sd_setImage(with: url)
    }
}
