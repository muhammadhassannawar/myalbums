# MyAlbums

### Overview

This repo includes a sample project that demonstrates the MVVM patterns on iOS using Combine for binding, Use Moya library for network abstraction layer, Use interactor layer and routing layer.

* Xcode 13.1
* Swift 5.5
* iOS 13+

in this project The main screen which contain a User Profile which contain user information and albums of photos, When you press on any album it navigates to the second screen which is an album details screen. You request the photos endpoint and pass album id as a parameter, then list the images in an Instagram-like grid. Also, there should be a search bar that you can filter within the album by the image title, when you start typing the screen should show only images that are related to this search query, When user select one of album photos it will present the photo with the ability to zoom in and out the photo and share it also.

<img src="https://i.postimg.cc/3JZ0bjN3/Simulator-Screen-Shot-i-Phone-13-Pro-Max-2022-05-28-at-14-46-00.png" width="210"> <img src="https://i.postimg.cc/pr0dBCVT/Simulator-Screen-Shot-i-Phone-13-Pro-Max-2022-05-28-at-14-46-16.png" width="210"> <img src="https://i.postimg.cc/xThfTccW/Simulator-Screen-Shot-i-Phone-13-Pro-Max-2022-05-28-at-14-46-28.png" width="210"> <img src="https://i.postimg.cc/76HDQzWT/Simulator-Screen-Shot-i-Phone-13-Pro-Max-2022-05-28-at-14-46-48.png" width="210">

#  Swift Package Manager
In this project includes some of useful library for iOS, such as:
* SDWebImage
* Alamofire
* Moya

# Included API

This project requires the following API:
Base URL: https://jsonplaceholder.typicode.com
- User: GET /users you can pick any user to start with (preferably random)
- Albums: GET /albums (userId as a parameter)
- Photos: GET /photos (albumId as a parameter)
